## Sorting Algorithms ##

#### bubble.c ####
This is a program based off of bubble sort. It allows you to specify sorting order using a function pointer.

Given an array with the members ``{Alex, brianne, Brianne, cat, alex, Cat}`` the function will allow you store the sort in
the following way:

* Case separated ``{Alex, Brianne, Cat, alex, brianne, cat}``
* Mixed case ``{Alex, alex, Brianne, brianne, Cat, cat}``
